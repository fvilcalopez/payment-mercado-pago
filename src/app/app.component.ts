import {Component, Input, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { MercadoPagoService } from './core/api/MercadoPagoService';
import {PaymentsService} from "./core/api/payments.service";
import { AlertDialogComponent } from './shared/utils/alert-dialog/alert-dialog.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'payments-app-vc';
  mercadoPagoService!: MercadoPagoService;
  total: any;
  user_id: any;
  codigodscto: any;
  upgrade:any;
  registerType:any;
  constructor(private route: ActivatedRoute,
              private paymentsService: PaymentsService,
              public dialog: MatDialog,
              private router: Router ) {  }

  ngOnInit(): void {
    this.route.queryParams.subscribe(
      queryParams => {
        console.log(queryParams);
        this.total = queryParams['total'];
        this.user_id = queryParams['user_id'];
        this.codigodscto = queryParams['codigodscto'];
        this.upgrade = queryParams['upgrade'];
        console.log(this.total);
        console.log(this.user_id);
        console.log(this.codigodscto);
        console.log(this.upgrade);
      });



  }




}
