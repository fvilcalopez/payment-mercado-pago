import { Subject } from 'rxjs';
// @ts-ignore
import { get } from 'scriptjs';
// @ts-ignore
declare function MercadoPagoSKD();
declare const MercadoPago: any;
export class MercadoPagoService {
  preferenceId: string | undefined;
  onSuccess = new Subject();
  onCancel = new Subject();
  onReady = new Subject();
  constructor(private relativePathToBase: string) {

  }

  public setPreferenceId(preferenceId: string | undefined) {
    this.preferenceId = preferenceId;
  }
  public loadDependencies() {
    get(this.relativePathToBase + '/assets/scripts/MercadoPago.js', () => {
      MercadoPagoSKD();
      this.onReady.next(null);
    });
  }

  public pay() {
    const mp = new MercadoPago('APP_USR-dc8e4ca9-2f6a-413d-875b-c68feb6d6731', {
      locale: 'es-PE'
    });

    // Inicializa el checkout
    const checkout = mp.checkout({
      preference: {
        id: this.preferenceId
      },
      autoOpen: true,
      onCancel: (e:any) => {
        console.log(e);
        console.log("Pago cancelado");
        this.onCancel.next(e);
      },
      onSubmit: (e:any) => {
        console.log(e);
        this.onSuccess.next(e);
      }
    });
  }
}
