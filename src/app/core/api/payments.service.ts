import {HttpClient, HttpParams} from "@angular/common/http";
import { Injectable } from "@angular/core";
// import { IPayment } from "./models/payment";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { environment } from "src/environments/environment";
import { IPayment } from "../models/payment";


@Injectable({
    providedIn: 'root'
  })


export class PaymentsService {
    private url = environment.apiHost ;
    constructor(private httpClient: HttpClient) { }

    createPreferenceMP(payment:IPayment):Observable<IPayment>{
        return this.httpClient.post<IPayment>(this.url + '/payments/mercado-pago',payment)
            .pipe();
    }

    createPayment(userId:number, payment:any):Observable<IPayment>{
        return this.httpClient.post<IPayment>(this.url + '/payments/register/' + userId, payment)
            .pipe();
    }

    obtenerCodigo(code:String):Observable<any>{
        return this.httpClient.get<any>(this.url + '/payments/coupon-code/search/' + code)
            .pipe();
    }

    upgradePayment(userId:number, payment:any):Observable<IPayment>{
        return this.httpClient.post<IPayment>(this.url + '/payments/upgrade/' + userId, payment)
        .pipe();
    }
    checkPaymentByUser(userId:number, registerTypeId:number):Observable<any>{
      let params = new HttpParams()
      params = params.append('user_id', userId);
      params = params.append('register_type_id', registerTypeId);
      console.log(params)
      return this.httpClient.get<any>(this.url + '/payments/check-payment-by-user', {params: params});
    }
}
