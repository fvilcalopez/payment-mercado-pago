

export interface IPayment {
    id_preference?: string;
    code?: string;
    register_type_id?: number;
    user_id?: number;
    payment_type_id?: number;
    checkout_status?: string;
    payment_price?: number;
    cupon_code?: string;
    total_payment?: number;
    coupon_code_id?: string;
}

export interface IReportPayment{
    id?: number;
    user_full_name: string;
    register_type: string;
    payment_type: string;
    created_at?: Date;
    updated_at?: Date;
    checkout_status: string,
    total_payment: number,
    coupon_code?: string,
}
