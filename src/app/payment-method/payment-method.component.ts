import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MercadoPagoService } from '../core/api/MercadoPagoService';
import {MatDialog} from "@angular/material/dialog";
import { PaymentsService } from '../core/api/payments.service';
import { AlertDialogComponent } from '../shared/utils/alert-dialog/alert-dialog.component';

@Component({
  selector: 'app-payment-method',
  templateUrl: './payment-method.component.html',
  styleUrls: ['./payment-method.component.scss']
})
export class PaymentMethodComponent implements OnInit {
  public loader: boolean = false;
  dialogRefAlert: any;

  culqiAPIKEY= "pk_test_1f9dc1b24f8d6ced";
  didPayPalScriptLoad = false;
  mercadoPagoService!: MercadoPagoService;

  @Input() total: any;
  @Input() user_id: any;
  @Input() codigodscto: any;
  @Input() upgrade:any;
  @Input() registerType:any;
  @Output() result = new EventEmitter<number>();
  constructor(private paymentsService: PaymentsService,
              public dialog: MatDialog) {
    console.log({total:this.total, user_id:this.user_id, codigodscto:this.codigodscto, upgrade:this.upgrade, registerType:this.registerType});
    this.mercadoPagoService= new MercadoPagoService('../..');
    this.mercadoPagoService.onSuccess.subscribe((a) => {console.log('susccess mercadoPagoService'); this.despuesPago(a, 2);});
    this.mercadoPagoService.onReady.subscribe((a) => {console.log('ready mercadoPagoService'); console.log(a);});
    this.mercadoPagoService.onCancel.subscribe((a) => {console.log('cancel mercadoPagoService'); this.despuesPagoError(a,2)});
  }

  ngOnInit( ): void {
    this.mercadoPagoService.loadDependencies();
  }

  openMercadoPagoCheckOut():void {
    this.paymentsService.checkPaymentByUser(this.user_id, this.registerType).subscribe(
      response=>{
        console.log(this.total);
        let payment = {id_preference:'', payment_price:this.total, user_id: this.user_id, payment_type_id:2, register_type_id:this.registerType}
        this.loader = true;
        this.paymentsService.createPreferenceMP(payment).subscribe(sucess=> {
          console.log(sucess.id_preference);
          this.mercadoPagoService.setPreferenceId(sucess.id_preference);
          this.mercadoPagoService.pay();
        },error => {
          this.loader = false;
          this.openAlertdialogs('alert-error',"Ocurrió un error con mercadoPago", 'Upss..', null, 'error');
          console.log(error);
        });
      }, error => {
        this.loader = false;
        this.openAlertdialogs('alert-error','El usuario ya cuenta con un pago', 'Upss..', null, 'error');
        console.log(error);
      }
    );
  }

  despuesPago(data: any, tipo: number){
    this.loader = false;
    // this.openAlertdialogs('alert-succesful',"Pago realizado con éxito", '¡Éxito!', null, 'success');
    console.log("Pago realizado con éxito",{data, tipo});
    let payment;
    let uId = this.user_id;
    if(uId == 0){
      this.openAlertdialogs('alert-error',"Ocurrió un error con el usuario y no se puede iniciar sesión automaticamente", 'Upss..', null, 'error');
      return;
    }
    if(this.codigodscto){
      payment = {
        'code': null,
        'payment_type_id' : tipo,
        'checkout_status' : '',
        'cupon_code' : this.codigodscto,
        'total_payment' : this.total
      };
    }else{
      payment = {
        'code': null,
        'payment_type_id' : tipo,
        'checkout_status' : '',
        'total_payment' : this.total
      };
    }
    switch(tipo){
      case 1:
        payment.code = data['paymentID'];
        payment.checkout_status = 'SUCCESS';
        break;
      case 2:
        let payment_id = data.find((post: { id: string;  value: any}) => {
          if(post.id == "payment_id")
            return post.value;
          else return null;
        });
        console.log(payment_id);
        payment.code = payment_id.value;
        payment.checkout_status = 'PENDING';
        break;
      case 3:
        payment.code = data['id'];
        payment.checkout_status = 'SUCCESS';
        break;
      default:
        this.loader = false;
        this.openAlertdialogs('alert-error', "Ocurrió un error al registrar en el pago al sistema", 'Upss..', null, 'error');
        return;
    }
    if(this.upgrade){this.upgradePayment(uId,payment)}else{this.crearPayment(uId, payment);}

  }

  crearPayment(uId:number, payment:any){
    this.loader = true;
    this.paymentsService.createPayment(uId, payment).subscribe(
      success => {
        this.loader = false;
        console.log(success);
        this.openAlertdialogs('alert-succesful', 'El pago fué registrado en el sistema con éxito', '¡Éxito!', null, 'success');
        this.cerrar(1);// this.autoLogin();
      },
      error => {
        this.loader = false;
        this.openAlertdialogs('alert-error', "Ocurrió un error al registrar en el pago al sistema", 'Upss..', null, 'error');
        // this.openAlertdialogs('alert-error', error.error.message ?? "Ocurrió un error", 'Upss..', error.error.subErrors?? error.error, 'error');
        console.log(error);
      }
    );
  }

  upgradePayment(uId:number,payment:any){
    this.loader = true;
    this.paymentsService.upgradePayment(uId,payment).subscribe(
      success=>{
        this.loader=false;
        console.log(success);
        this.openAlertdialogs('alert-succesful', 'El pago fué registrado en el sistema con éxito', '¡Éxito!', null, 'success');
        this.cerrar(1);
      },
      error =>{
        this.loader = false;
        this.openAlertdialogs('alert-error', "Ocurrió un error al registrar en el pago al sistema", 'Upss..', null, 'error');
      });
  }

  despuesPagoError(data: any, tipo: number){
    this.loader = false; console.log(data);
    let msg = tipo == 1? 'Paypal' : tipo == 2 ? 'MercadoPago' : 'Culqi';
    //this.openAlertdialogs('alert-error',"Ocurrió un error con el metodo de pago de " + msg, 'Upss..', null, 'error');
  }

  openAlertdialogs(responseService: string, message: string, header: string, error: any, icon ?: string, icon_color ?: string) {
    this.dialogRefAlert = this.dialog.open(AlertDialogComponent, {
      data: {
        responseService: responseService,
        message: message,
        header: header,
        error: error,
        icon: icon
      }
    });
  }

  cerrar(tipo:any =  null) {
    this.result.emit(tipo);
  }

}
