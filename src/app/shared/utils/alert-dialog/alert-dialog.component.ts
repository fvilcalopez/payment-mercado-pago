import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";

@Component({
  selector: 'app-alert-dialog',
  templateUrl: './alert-dialog.component.html',
  styleUrls: ['./alert-dialog.component.scss']
})
export class AlertDialogComponent implements OnInit {
  @Output() responseAcceptReject = new EventEmitter<boolean>();
  public responseService!: string;
  public message!: string;
  public header!: string;
  public errors!: any[];
  public errormessage!: string;
  public icon!: string;
  public icon_color!: string;
  private translate: any;
  constructor(public dialogRef: MatDialogRef<AlertDialogComponent>,
              @Inject(MAT_DIALOG_DATA) private data: any) {
    this.responseService = data.responseService;
    this.message = data.message;
    this.header = data.header;
    this.errors = this.setErrors(data.error);
    this.errormessage = data.errormessage;
    this.icon = data.icon;
    this.icon_color = data.icon_color;
    this.translate = {
      'id': 'Identificador'
    };
  }

  ngOnInit() {

  }

  confirmacionAlerta(action: boolean){
    this.responseAcceptReject.emit(action);
    this.dialogRef.close();
  }

  getFiled(field: string): string{
    if (this.translate[field]) {
      return this.translate[field];
    }
    return field;
  }

  setErrors(errors: any): any[]{
    let aux: any[] = [];
    for (const key in errors) {
      let element = errors[key];
      aux.push({field:element.field ?? element, path:element.field ?? null});
    }
    return aux;
  }

}
